#!/bin/bash
set -e

# Build package
export NRP_INSTALL_MODE=user
make set-nrp-version

pushd hbp_nrp_virtual_coach
python3 setup.py sdist bdist_wheel
popd

cp hbp_nrp_virtual_coach/dist/*.whl ./