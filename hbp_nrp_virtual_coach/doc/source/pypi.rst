.. _virtual-pypi:

.. sectionauthor:: Viktor Vorobev <vorobev@in.tum.de>

.. seealso::

    :ref:`User manual<virtual_coach_intro>` / 
    :ref:`Tutorials<virtual-coach-tutorial>` / 
    :ref:`Developer pages<virtual_coach_dev_space>`

Stand-alone installation
========================

The Virtual Coach is packed and published to PyPi repository.

The official releases can be found `here <https://pypi.org/project/pynrp/>`_ and you can install it as 

.. code-block:: bash

    pip3 install pynrp


The intermediate packages are stored in internal PyPi repository with developer-only access. One requires the username and the password for the developers Nexus server to get them. The following command will help to install the specific version

.. code-block:: bash

    pip3 install pynrp==<version> --index-url https://<user>:<pass>@nexus.neurorobotics.ebrains.eu/repository/nrp-pypi/simple/ --trusted-host nexus.neurorobotics.ebrains.eu --use-deprecated=legacy-resolver

where

* :code:`<user>` is your Nexus username;
* :code:`<pass>` is your Nexus password;
* :code:`<version>` is a package version, that is determined as :code:`git describe --tags --always` of your commit.

You can browse the available versions `here <https://nexus.neurorobotics.ebrains.eu/#browse/browse:nrp-pypi-internal>`_.
