.. _virtual-coach-api:

.. sectionauthor:: Viktor Vorobev <vorobev@in.tum.de>

.. seealso::

    :ref:`User manual<virtual_coach_intro>` / 
    :ref:`Tutorials<virtual-coach-tutorial>` / 
    :ref:`Developer pages<virtual_coach_dev_space>`

Python developer API
====================

.. toctree::
   :maxdepth: 4

   apidoc/pynrp