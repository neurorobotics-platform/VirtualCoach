.. _virtual_coach_dev_space:

.. sectionauthor:: Viktor Vorobev <vorobev@in.tum.de>

.. seealso::

    :ref:`User manual<virtual_coach_intro>` / 
    :ref:`Tutorials<virtual-coach-tutorial>` / 
    :ref:`Code API reference<virtual-coach-api>`

Virtual Coach developer space
=============================

.. toctree::
    :maxdepth: 2

    python_api
    pypi