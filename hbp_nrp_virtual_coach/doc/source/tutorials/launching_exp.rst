.. _virtual-coach-tutorials-launch:

..  sectionauthor:: Eloy Retamino <retamino@ugr.es>

Launching an Experiment from the Virtual Coach
==============================================

Starting the Virtual Coach
^^^^^^^^^^^^^^^^^^^^^^^^^^

Local source installation
~~~~~~~~~~~~~~~~~~~~~~~~~

In case you have installed NRP from the :ref:`source <source-installation>`, there is a special alias for running the Virtual Coach. For this alias to work, :code:`$HBP/user-scripts/nrp_aliases` has to be sourced in your *bash.rc*. The ``configure_nrp`` script takes care of that automatically. The Virtual Coach alias is :code:`cle-virtual-coach` and can be run in three different ways:

There are different ways to start a Virtual Coach instance. With a local :abbr:`NRP (Neurorobotics Platform)` install you can use the alias :code:`cle-virtual-coach` in three different ways:

1. **Launch a Jupyter Notebook session**

   To launch a Jupyter Notebook session just run :code:`cle-virtual-coach jupyter notebook` in a terminal. Of course, Jupyter Notebook has to be installed prior.

2. **Launch an interactive python interpreter session**

   To launch an interactive python interpreter session just run :code:`cle-virtual-coach python` in a terminal.

3. **Launch a python script with arguments**

   To launch a python script *foo.py* with arguments *a b c* just run :code:`cle-virtual-coach foo.py a b c`.

This information is also available in the alias help :code:`cle-virtual-coach -h`.


Local Docker installation
~~~~~~~~~~~~~~~~~~~~~~~~~

You can also use Virtual Coach with the local :ref:`Docker installation <docker-installation>`. In order to do that, you should follow these steps:

1. Install the latest NRP Docker version with the command of :code:`./nrp_installer.sh install latest` the script exposes port 8888 by default for the Jupyter.

2. Run :code:`./nrp_installer.sh connect_backend` which opens a terminal inside backend container.

3. Run the command of :code:`cle-virtual-coach jupyter notebook "--ip=0.0.0.0"` inside backend container to start Jupyter.

This will give you a URL like :code:`http://localhost:8888/?token=f027e20b23dd04d34ec827c6ff8ee402ccdf4c775d440766` that you should open with your browser.

If you want to specify another port for Jupyter, the steps above change as follows:

1. When installing nrp you need to add :code:`-np/--notebook_port <port_number>` to install command. For example, the command of :code:`./nrp_installer.sh -np 9875 install latest` will install nrp with port 9875 to be exposed for Jupyter.

2. Run :code:`./nrp_installer.sh connect_backend` which opens a terminal inside backend container.

3. Run the command of :code:`cle-virtual-coach jupyter notebook "--ip=0.0.0.0" --port=9875` inside backend container to start Jupyter on port specified in step 1. This command yields a URL that allow you to access the Jupyter notebook run on the container.

To **test** that the installation was successful, create a new notebook and run this line of code:

.. code-block:: python

    from pynrp import virtual_coach

if you get no error running this cell, your Jupyter notebook is ready.


Launching a Simulation
^^^^^^^^^^^^^^^^^^^^^^
The first thing we need to do is to import the Virtual Coach and create a VirtualCoach instance connected to an NRP server.

.. code-block:: python

    from pynrp.virtual_coach import VirtualCoach
    vc = VirtualCoach(environment='http://localhost:9000', storage_username='nrpuser', storage_password='password')

Here we chose the local machine as the NRP server. The Virtual Coach can connect to NRP servers running in other locations by setting the *environment* parameter to the proper URL. For example, it is possible to connect to the NRP server running at *https://nrp-legacy.neurorobotics.ebrains.eu* by executing this command:

.. code-block:: python

    vc = VirtualCoach('https://nrp-legacy.neurorobotics.ebrains.eu', oidc_username='<your-hbp-username>', oidc_password='<your-hbp-password>')

Notice that parameters *storage_username* and *storage_password* are replaced with *oidc_username* and *oidc_password*. The latter must be used when EBRAINS :term:`OIDC` authentication is required, the former are used otherwise.

.. note:: Prior using Virtual Coach with the online NRP servers, you should login at least once to that online server with your EBRAINS account in order to give the user consent.

Once we created a VirtualCoach instance, we can use it to check out the current state of our environment. We can see a list of the available experiments to run, a list of the available servers to run experiments on, and a list of the currently running experiments.

.. code-block:: python

    vc.print_templates()
    vc.print_available_servers()
    vc.print_running_experiments()

After making sure our experiments exist and enough resources are available, we can attempt to launch an experiment. In this next section we will launch the Template Husky experiment. We first need to clone the experiment in our storage. To launch a specific experiment, we need to specify its configuration name, which we can get from the cloned experiment list.

.. code-block:: python

    exp_id = vc.clone_experiment_to_storage('ExDTemplateHusky')
    vc.print_cloned_experiments()
    sim = vc.launch_experiment(exp_id)


Launching an experiment can take some time and once it's been successfully launched, we'll get a `Simulation Successfully Created` log.

We can also make sure that the experiment has been launched by querying the Virtual Coach for currently running experiments.

----------------------------------

Read more about the **interaction with the experiment throught the Virtual Coach** in the :ref:`following tutorial <virtual-coach-tutorials-interact>`.
