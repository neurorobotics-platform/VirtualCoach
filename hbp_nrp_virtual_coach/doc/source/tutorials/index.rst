.. _virtual-coach-tutorial:

..  sectionauthor:: Eloy Retamino <retamino@ugr.es>

.. seealso::

    :ref:`User manual<virtual_coach_intro>` / 
    :ref:`Code API reference<virtual-coach-api>` / 
    :ref:`Developer pages<virtual_coach_dev_space>`

.. this page is included in the whole docs

Virtual Coach Tutorial
======================

.. toctree::

    launching_exp
    interacting_exp
