# third-party dependencies
future
texttable==0.8.7
progressbar2==3.52.1
httplib2==0.12.1
pandas==1.4.4
jupyter==1.0.0
requests
ipykernel==5.3.2
mechanize
roslibpy==1.1.0
service_identity