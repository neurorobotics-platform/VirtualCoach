# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
Unit tests for the Virtual Coach config parser.
"""

from pynrp.config import Config

from unittest.mock import patch
import unittest

import copy
import json
import os

class TestConfig(unittest.TestCase):

    def setUp(self):
        # load the config file so we can compare the update (already validated above)
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../config.json')
        with open(path) as conf_file:
            self._conf = json.load(conf_file)

    @patch('pynrp.config.os.path.isfile')
    def test_missing_config(self, mock_isfile):
        # missing config.json
        mock_isfile.return_value = False
        self.assertRaises(IOError, Config, 'localhost')

    @patch('pynrp.config.os.access')
    def test_unreadable_config(self, mock_access):
        # unreadable config.json
        mock_access.return_value = False
        self.assertRaises(IOError, Config, 'localhost')

    @patch('pynrp.config.json.load')
    def test_malformed_config(self, mock_load):
        # malformed config.json
        mock_load.side_effect = Exception('fake exception')
        self.assertRaises(Exception, Config, 'localhost')

    def test_update_proxy_services(self):
        config = Config('localhost')
        for k, v in config['proxy-services'].items():
            self.assertEqual(v, '%s/proxy/%s' % ('localhost', self._conf['proxy-services'][k]))

    def test_trailing_slash(self):
        self.assertEqual(Config('localhost:8080'), Config('localhost:8080/'))

    @patch('pynrp.config.json.load')
    def test_validation_missing_key(self, mock_load):
        # missing section
        conf = copy.deepcopy(self._conf)
        conf.pop('oidc')
        mock_load.return_value = conf
        self.assertRaises(KeyError, Config, 'localhost')

    @patch('pynrp.config.json.load')
    def test_validation_missing_value(self, mock_load):
        # missing sub-key/value
        conf = copy.deepcopy(self._conf)
        conf['oidc'].pop('user')
        mock_load.return_value = conf
        self.assertRaises(ValueError, Config, 'localhost')