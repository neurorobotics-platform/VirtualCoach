1. Description

  This example performs a high level integration test of almost all available Virtual Coach
  functionality to ensure compatibility with the NRP backend beyond what simple unit tests
  can achieve. It performs a sequence of events and will abort upon the first failure.

  This script currently only supports integration testing on a local installation, but does
  support backends with OIDC enabled or disabled, see command line arguments below.


2. Command Line Argument Reference

  -h:

    (optional) print usage information (essentially this information, but always up to date)

  -v --verbose:

    (optional) enable all Virtual Coach logging during execution, this is disabled by default to
    provide cleaner output

  -u --oidc-username:

    (optional) provide an OIDC username to login if OIDC is enabled on the local backend, you
    may be prompted for a password during script execution if a valid OIDC token is not available
    (this will not cause any issues if OIDC is disabled on the backend)

  -s --storage-username:

    (optional) provide a local Storage Server username to login and have access to the cloned
    experiments. You will be prompted for a password


3. Running Integration Tests

  3.1 Start your local NRP backend installation (you do not need to start the frontend):

    refer to latest NRP documentation

  3.2 Launch the script in the VirtualCoach environment:

    cle-virtual-coach $HBP/VirtualCoach/examples/integration_test/it.py <options>

  3.3 Examine the results table manually or check the return code of the script.
